#!/bin/bash
mkdir -p /var/backup-sommteck.net
mkdir -p /var/backup-sommteck.net/Archives
mkdir /var/backup-sommteck.net/databases
mkdir /var/backup-sommteck.net/httpdocs
cp -a /var/www/vhosts/sommteck.net/httpdocs/. /var/backup-sommteck.net/httpdocs
mysqldump wordpress_b > /var/backup-sommteck.net/databases/wordpress_b.sql
mysqldump piwigo-gallery > /var/backup-sommteck.net/databases/piwigo-gallery.sql
DT=$(date +%Y-%m-%d_%H:%M:%S)
FILENAME_NEW=backup-sommteck.net_$DT.tar.gz
tar -czf /var/backup-sommteck.net/Archives/$FILENAME_NEW /var/backup-sommteck.net/databases /var/backup-sommteck.net/httpdocs
echo "$DT $FILENAME_NEW created" >> /var/backup-sommteck.net/Backups.log
rm -rf /var/backup-sommteck.net/databases
rm -rf /var/backup-sommteck.net/httpdocs

DT=$(date +%Y-%m-%d_%H:%M:%S)
FILE_COUNT=$(find /var/backup-sommteck.net/Archives -type f | wc -l)
if [ $FILE_COUNT -gt 4 ]
	then
		while [ $FILE_COUNT -ge 5 ]
		do
			## How do I find the oldest file in a directory tree? This is the GNU way. (GNU find)
			OLDEST_ARCHIVE=$(find /var/backup-sommteck.net/Archives -type f -printf '%T+ %p\n' | sort | head -n 1)
			FILENAME_OLD=$(echo $OLDEST_ARCHIVE | cut -d" " -f2)

			## How do I find the oldest file in a directory tree? This is the BSD/Unix way. (BSD/Unix find)
			#OLDEST_ARCHIVE=$(find /var/backup-sommteck.net/Archives -type f -print0 | xargs -0 ls -ltr | head -n 1)
			#FILENAME_OLD=$(echo $OLDEST_ARCHIVE | cut -d" " -f9)

			rm $FILENAME_OLD
			echo "$DT $FILENAME_OLD deleted" >> /var/backup-sommteck.net/Backups.log
			
			## Ist die Neudeklaration der numerischen Variable $FILE_COUNT wirklich nötig?
			FILE_COUNT=$(find /var/backup-sommteck.net/Archives -type f | wc -l)
		done
	else
		echo "$DT No archive deleted" >> /var/backup-sommteck.net/Backups.log
fi
